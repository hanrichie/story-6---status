from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
    class Meta:
        model = Status
        fields = ('status',)
        widgets = {
            'status' : forms.Textarea(attrs={'placeholder':'Write your status...'}),
        }