from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import Status

# Create your views here.
def statusForm(request):
    form = StatusForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        form.save()
        return redirect('statusForm:statusForm')

    response = {
        'form' : form,
        'status' : Status.objects.all().order_by('-time') if Status.objects.all().order_by('-time') else None,
    }
    return render(request, 'statusPage.html', response)