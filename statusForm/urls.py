from django.urls import path
from . import views

app_name = 'statusForm'

urlpatterns = [
    path('', views.statusForm, name='statusForm'),
]