from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from .models import Status
from .views import statusForm
from .apps import StatusformConfig

import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# Create your tests here.
class UnitTest(TestCase):
    def test_status_form_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_status_form_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, statusForm)

    def test_status_exist(self):
        Status.objects.create(status="INI STATUS")
        self.assertEqual(Status.objects.all().count(), 1)

    def test_app_name(self):
        self.assertEqual(StatusformConfig.name, 'statusForm')
        self.assertEqual(apps.get_app_config('statusForm').name, 'statusForm')
    
    def test_status_form_post_request(self):
        test = {'status':'STATUS'}
        request = Client().post('/', test, follow=True)
        self.assertEqual(Status.objects.count(), 1)

        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response,'STATUS', count=1, html=True)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        self.options = webdriver.ChromeOptions()
        self.browser = webdriver.Chrome(options=self.options, executable_path='./chromedriver.exe')
        return super().setUp()

    def tearDown(self):
        self.browser.quit()
        return super().tearDown()

    def test_can_submit_status_form(self):
        self.browser.get(self.live_server_url)
        time.sleep(2)

        input = self.browser.find_element_by_name("status")
        input.send_keys("INI TEST PERTAMA")
        time.sleep(2)
        btn = self.browser.find_element_by_class_name("btn")
        btn.click()
        time.sleep(2)

        input = self.browser.find_element_by_name("status")
        input.send_keys("INI TEST KEDUA")
        time.sleep(2)
        btn = self.browser.find_element_by_class_name("btn")
        btn.click()
        time.sleep(3)
